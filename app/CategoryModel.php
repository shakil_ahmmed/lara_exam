<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class CategoryModel extends Model
{
     use SoftDeletes;
     protected $table="category";
     protected $primaryKey="category_id";
     protected $fillable=['category_name','category_slug','description','category_id'];

     protected $dates = ['deleted_at'];

     public function validation($id=0)
     {
       return [
            'category_name'=>'required|max:50|unique:category,category_name,'.$id.',category_id',
                 'category_slug'=>'required|max:50',
                 'description'=>'required'
     	    ];
     }
}
