<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         $this->middleware('auth');
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->is_admin==1)
        {
            Session::put('admin','admin');
            return view('Admin.home');
        }
        else
        {
            Session::put('user','user');
            return redirect('/Examination');
        }
        
        
    }
}
