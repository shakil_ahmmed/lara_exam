<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoryModel;
use Validator;
use Toastr;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Admin.Category.category');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category_data=CategoryModel::all();
        return view('Admin.Category.category_list',['category_data'=>$category_data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category_model=new CategoryModel;
        $validation=Validator::make($request->all(),$category_model->validation());
        if($validation->fails())
        {
            return back()->withInput()->withErrors($validation);
        }
        else
        {
            $requested_data=$request->all();
            $requested_data=array_add($requested_data,'category_id',time());
            $category_model->fill($requested_data)->save();
            Toastr::success('Category Successfully Added', '', ["positionClass" => "toast-top-center"]);
            return back();
        }
    }

    public function category_trash()
    {
        $category_data = CategoryModel::onlyTrashed()->get();
        return view('Admin.Category.category_trashed_list',['category_data'=>$category_data]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $category_restore=CategoryModel::withTrashed()->findOrFail($id)->restore();
       Toastr::success('Category Restored Successfully','',["positionClass" => "toast-top-center"]);
        return back();

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category_edit_data=CategoryModel::findOrFail($id);
        return view('Admin.Category.category_edit',['category_edit_data'=>$category_edit_data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category_model=CategoryModel::findOrFail($id);
        $validation=Validator::make($request->all(),$category_model->validation($id));
        if($validation->fails())
        {
            return back()->withInput()->withErrors($validation);
        }
        else
        {

            $category_model->fill($request->all())->save();
            Toastr::success('Category Successfully Added', '', ["positionClass" => "toast-top-center"]);
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category_delete=CategoryModel::findOrFail($id);
        $category_delete->delete();
        Toastr::success('Category Trashed Successfully','',["positionClass" => "toast-top-center"]);
        return back();
    }
}
