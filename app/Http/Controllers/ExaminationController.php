<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
class ExaminationController extends Controller
{
     public function __construct()
    {
        //$this->middleware(['auth','authenticate']);
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_check=Session::get('user');
        if($user_check=='user')
        {
           return view('Examination.index'); 
        }
        else
        {
            return abort(404);
        }
        
    }
}
