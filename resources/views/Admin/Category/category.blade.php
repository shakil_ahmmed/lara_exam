 @extends('Admin.index')
 @section('links','category')
 @section('title','Category')
 @section('content','Category')
 @section('main_content')
<link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">

 <div class="col-lg-2 col-md-2"></div>
 <div class="col-lg-8 col-md-7">
 	<div>
 		@if($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i>
                   {{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
      @endif
 	</div>
 	<script src="http://cdn.bootcss.com/jquery/2.2.4/jquery.min.js"></script>
   <script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
   {!! Toastr::message() !!}

                        <div class="card">
                            <div class="header">
                                <h4 class="title">Add Category</h4>
                            </div>
                            <div class="content">
                                {{Form::open(['url'=>'/category'])}}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            {{Form::label('Category Name')}}
                                            {{Form::text('category_name','',['class'=>'form-control border-input category_name','title'=>'category_name'])}}
                                               
                                            </div>
                                        </div>
                                      </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                       {{Form::label('Category Slug')}}
                                       {{Form::text('category_slug','',['class'=>'form-control border-input category_slug','readonly'=>'readonly', 'title'=>'category_slug'])}}
                                               
                                            </div>
                                        </div>
                                   
                                    </div>

                                    

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                
                                                {{Form::label('Description')}}
                                                {{Form::textarea('description','',['class'=>'editor form-control border-input','rows'=>'5','title'=>'description'])}}
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                      {{Form::submit('Save',['class'=>'btn btn-info btn-fill btn-wd'])}}
                                    </div>
                                    <div class="clearfix"></div>
                               {{Form::close()}}
                            </div>
                        </div>
                    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   
      <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script type="text/javascript">
    	$('.editor').ckeditor();
    	$(document).ready(function(){
    		$(".category_name").keyup(function(){
    			var category_name=$(this).val();
    			var slug=category_name.toLowerCase();
                $(".category_slug").val(slug.replace(/\s+/g, "-"));
    		
    		});
    	});
        
 
    </script>
 @stop