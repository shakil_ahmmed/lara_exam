 @extends('Admin.index')
 @section('links','category/create')
 @section('title','Manage Category')
 @section('content','Manage Category')
 @section('main_content')
 <div class="content">
 	<link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
 	<script src="http://cdn.bootcss.com/jquery/2.2.4/jquery.min.js"></script>
   <script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
   {!! Toastr::message() !!}
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Manage Category</h4>
                               
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-striped" id="myTable">
                                    <thead>
                                        <th>ID</th>
                                    	<th>Category Name</th>
                                    	<th>Category Slug</th>
                                    	<th>Description</th>
                                    	<th>Action</th>
                                    </thead>
                                    <tbody>
                                    	@foreach($category_data as $key=>$category_data_value)
                                        <tr>
                                        	<td>{{$key+1}}</td>
                                        	<td>{{$category_data_value->category_name}}</td>
                                        	<td>{{$category_data_value->category_slug}}</td>
                                        	<td>{{$category_data_value->description}}</td>
                                        	<td>
                                         {{Form::open(['url'=>"category/$category_data_value->category_id",'method'=>'GET'])}}
                                        		<button class="btn btn-danger"><i class="fa fa-recycle" aria-hidden="true"></i></button>
                                            {{Form::close()}}
                                        	</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

 @stop