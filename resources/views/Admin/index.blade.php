@include('layouts.admin_header')
@include('layouts.admin_sidebar')
@include('layouts.admin_navbar')
@yield('main_content')
@include('layouts.admin_footer')
@include('layouts.admin_js')

